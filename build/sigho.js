var SigHo = (function () {

    if (window.hasOwnProperty('speechSynthesis')) {
        speechSynthesis.getVoices();
    } else {
        console.error("SigHo needs Speech Synthesis API to speak.");
    }

    if (window.hasOwnProperty('webkitSpeechRecognition')) {
        this.SigHoWebkitSpeechRecognition = new window.webkitSpeechRecognition();
    } else {
        console.error("SigHo needs Speech Synthesis API to listen.");
    }
});
